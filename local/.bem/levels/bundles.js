var PATH = require('path'),
    environ = require('bem-environ'),
    getTechResolver = environ.getTechResolver;

environ.PRJ_ROOT = PATH.resolve('./');

var PRJ_ROOT = environ.PRJ_ROOT,
    PRJ_TECHS = PATH.resolve(PRJ_ROOT, 'local/.bem/techs/v2'),
    BEMCORE_TECHS = PATH.resolve(environ.PRJ_ROOT, 'node_modules/bem/lib/techs/v2');

exports.baseLevelPath = require.resolve('./blocks');

exports.getTechs = function() {
    var techs = this.__base();

    // Use techs from lib bem-core
    ['browser.js+bemhtml', 'html'].forEach(getTechResolver(techs, PRJ_TECHS));

    return techs;
};

// Create bundles in bemjson.js tech
exports.defaultTechs = ['bemjson.js'];
