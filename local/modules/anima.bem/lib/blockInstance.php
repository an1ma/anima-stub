<?php
/**
 * Created by PhpStorm.
 * User: mbakirov
 * Date: 21.04.15
 * Time: 13:43
 */

namespace Anima\Bem;


class BlockInstance {
    /**
     * @var DependencyInstance $deps
     */

    var $isLoaded = false;
    var $blockName;
    var $path;

    var $elems;
    var $mods;

    var $fullPath;

    var $js = array();
    var $css = array();
    var $deps;

    function __construct($__blockName, $__elemName = false)
    {
        $this->blockName = strtolower($__blockName);
        $blocksDir = array(
            '/bitrix/js/anima.bem/blocks/',
            '/local/blocks/',
        );

        foreach($blocksDir as $k => $dir)
        {
            $blockPath = $_SERVER['DOCUMENT_ROOT'].$dir.$this->blockName;
            if(is_dir($blockPath))
            {
                $this->fullPath = $blockPath;
                $this->path = str_replace($_SERVER['DOCUMENT_ROOT'], '', $blockPath.'/');
                break;
            }
        }
        //if(!$this->fullPath || $this->path) return;


        $this->getBlockDeps();
        $this->findBlockFiles();
        $this->findMods();
        $this->findModsFiles();
        $this->findElems();

        return $this;
    }

    function getBlockDeps()
    {
        $this->deps = new DependencyInstance($this);
    }

    function findBlockFiles()
    {
        $this->grabFiles($this->fullPath);
    }

    function findElems()
    {
        $dirs = array_filter(glob($this->fullPath.'/_*'), 'is_dir');
        foreach($dirs as $k => $path)
        {
            $dirName = str_replace($this->fullPath.'/', '', $path);
            if(strpos($dirName, '__') === false) continue;
            $this->elems[] = preg_replace("/^__/", '', $dirName);
        }
    }
    function findMods()
    {
        $dirs = array_filter(glob($this->fullPath.'/_*'), 'is_dir');
        foreach($dirs as $k => $path)
        {
            $dirName = str_replace($this->fullPath.'/', '', $path);
            if(strpos($dirName, '__') !== false) continue;
            $this->mods[] = preg_replace("/^_/", '', $dirName);
        }
    }

    function findModsFiles()
    {
        $dirs = array_filter(glob($this->fullPath.'/_*'), 'is_dir');
        foreach($dirs as $k => $path)
        {
            $dirName = str_replace($this->fullPath.'/', '', $path);
            if(strpos($dirName, '__') !== false) continue;
            $this->grabFilesMod($path, $dirName);
        }
    }

    function load()
    {
        if($this->isLoaded) return;

        //echo("<pre>");print_r('loadBlockFilesFromJson: '.$this->blockName);echo("</pre>");
        foreach($this->js as $js) {
            $GLOBALS['APPLICATION']->AddHeadScript($this->path.$js);
        }
        foreach($this->css as $css) {
            $GLOBALS['APPLICATION']->SetAdditionalCSS($this->path.$css);
        }
        $this->isLoaded = true;
    }

    function grabFiles($path)
    {
        foreach(scandir($path) as $key => $file) {
            if($key < 2) continue;

            if(preg_match("/\.min\.js/", $file)) {
                $this->js[] = $file;
                continue;
            }
            if(preg_match("/\.css$/", $file)) {
                $this->css[] = $file;
                continue;
            }
        }
    }

    function grabFilesMod($path, $modName)
    {
        foreach(scandir($path) as $key => $file) {
            if($key < 2) continue;

            if(preg_match("/\.min\.js/", $file)) {
                $this->js[] = $modName.'/'.$file;
                continue;
            }
            if(preg_match("/\.css$/", $file)) {
                $this->css[] = $modName.'/'.$file;
                continue;
            }
        }
    }
}