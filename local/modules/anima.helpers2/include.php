<?
\Bitrix\Main\Loader::registerAutoLoadClasses(
	"anima.helpers2",
	array(
		"Anima\\Helpers\\Image" => "lib/image.php",
		"Anima\\Helpers\\Url" => "lib/url.php",
		"Anima\\Helpers\\UHelpers" => "lib/UHelpers.php",
		"Anima\\Helpers\\Common" => "lib/common.php",
		"Anima\\Helpers\\String" => "lib/string.php",
	)
);