<?php
/**
 * Created by PhpStorm.
 * User: mbakirov
 * Date: 27.10.14
 * Time: 6:30
 */

namespace Anima\Bem;

/**
 * Class Common
 * @package Anima\Bem
 */
class Common {
    /**
     * method, that fires on each page hit when module installed
     */
    public static function init() {
        if(\COption::GetOptionString("anima.bem", "autoload_module", 1)) {
            self::autoLoad();
        }
    }

    /**
     * Autoloads helpers module
     *
     * @return bool
     */
    public static function autoLoad() {
        return \Bitrix\Main\Loader::includeModule('anima.bem');
    }
}